//package roman;// This is for Functional-based approach test
//
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.junit.runner.RunWith;
//import org.junit.runners.JUnit4;
//
//import static org.junit.Assert.*;
//
///**
// * Tips for Functionality-based approach:
// * 1. identify the characteristics that correspond to the intended functionality
// * 2. incorporate domain and SEMANTIC knowledge
// * Tag: functionality-based
// */
//@RunWith(JUnit4.class)
//public class RomanConverterFunctionalTest {
//    /*************************************  Initialization *****************************************/
//    private RomanConverter rc ;
//    @Before
//    public void init() {
//        rc = new RomanConverter();
//    }
//    @Test
//    public void empty()
//    {
//        assertNotNull(rc);
//    }
//
//    /*************************************  toRoman Tests *****************************************/
//    /*
//    * Characteristics :  we know the range of the Roman number, based on this functionality, we partition the input space:
//                          num>3999 | 3999>= num >= 1 | 1>num
//    *  Values:                4000 |  3999, 1       | 0
//    */
//    @Rule
//    public ExpectedException exception = ExpectedException.none();
//    @Test
//    public void outRange0() {
//        exception.expect(IllegalArgumentException.class);
//        exception.expectMessage("number out of range (must be 1..3999)");
//        rc.toRoman(0);
//    }
//    @Test
//    public void outRange4000(){
//        exception.expect(IllegalArgumentException.class);
//        exception.expectMessage("number out of range (must be 1..3999)");
//        rc.toRoman(4000);
//    }
//    @Test
//    public void inRange1(){
//        assertEquals("I",rc.toRoman(1));
//    }
//    @Test
//    public void inRange3999(){
//        assertEquals("MMMCMXCIX",rc.toRoman(3999));
//    }
//
//    /*
//    Case-sensitive characteristic:
//        binary: test the whether the returned results are uppercase or not
//        values: 1
//     */
//    @Test
//    public void caseSensitiveTrue()
//    {
//        assertEquals("I",rc.toRoman(1));
//    }
//    @Test
//    public void caseSensitiveFalse()
//    {
//        assertNotEquals("i",rc.toRoman(1));
//
//    }
//
//    /*************************************  fromRoman Tests *****************************************/
//    /*
//    same as before, we know the range then partition the input space as following:
//    characteristics: str > MMMCMXCIX| str == MMMCMXCIX, str == I | str == ""
//     */
//    @Test
//    public void outRangeRight()
//    {
//        exception.expect(IllegalArgumentException.class);
//        exception.expectMessage("Invalid Roman numeral: MMMMMMMMMMMMMMMMM");
//        rc.fromRoman("MMMMMMMMMMMMMMMMM");
//    }
//    @Test
//    public void outRangeLeft()
//    {
//        exception.expect(IllegalArgumentException.class);
//        exception.expectMessage("Invalid Roman numeral: -I");
//        rc.fromRoman("-I");
//    }
//    @Test
//    public void inRangeStr1()
//    {
//
//        assertEquals(1,rc.fromRoman("I"));
//    }
//    @Test
//    public void inRangeStr3999()
//    {
//        assertEquals(3999,rc.fromRoman("MMMCMXCIX"));
//    }
//
//    /*
//    Case-sensitive characteristic:
//        binary: test the whether the input are uppercase or not
//        values: I/i
//     */
//    @Test
//    public void caseSensitiveStrTrue()
//    {
//        assertEquals(1,rc.fromRoman("I"));
//    }
//    @Test
//    public void caseSensitiveStrFalse()
//    {
//        exception.expect(IllegalArgumentException.class);
//        exception.expectMessage("Invalid Roman numeral: i");
//        rc.fromRoman("i");// this will fail
//    }
//
//    /*************************************  fromRoman and toRomanTests *****************************************/
//    /*
//    In this case, we still don't use the program source, however we know the functionality of the program source;
//    The following tests are based on this observation:
//    Characteristic:
//        binary: fromRoman(toRoman(n)) == n, for all n in 1..3999
//    Values:
//        n == 1
//     */
//    @Test
//    public void trueConverter()
//    {
//        String str=rc.toRoman(1);
//        assertEquals(1,rc.fromRoman(str));
//    }
//    @Test
//    public void falseConverter()
//    {
//        String str=rc.toRoman(1);
//        assertNotEquals(2,rc.fromRoman(str));
//    }
//
//}
//
