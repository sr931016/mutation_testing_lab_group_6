//package roman;// This is for Interface-based approach test
//import org.junit.Before;
//import org.junit.Test;
//import roman.RomanConverter;
//import static junit.framework.TestCase.fail;
//import static org.junit.Assert.*;
//
///**
// * Tips for Interface-based approach:
// * 1. relies mostly on SYNTAX of that parameter
// * 2. ignores the relationships among parameters
// * 3. characteristics should be based on the input domain
// * tag: interface-based
// */
//
//public class RomanConverterInterfaceTest {
//    /*************************************  Initialization *****************************************/
//    private RomanConverter rc ;
//    public int num;
//    public String str;
//    @Before
//    public void init() {
//        rc = new RomanConverter();
//        num = 0;
//        str ="";
//    }
//    @Test
//    public void empty()
//    {
////        RomanConverter rc2 = new RomanConverter();
////        System.out.println("test " + rc2);
//        assertNotNull(rc);
//        //System.out.println("test " + num);
////        assertNotNull(num);
////        System.out.println("string" + str);
////        assertNotNull(str);
//    }
//
//    /*************************************  toRoman Tests *****************************************/
//    /*
//    Characteristics: relation between 1 and 3999
//                     num < 1 | num = 1 | 1 < num < 3999 | num = 3999 | num > 3999
//             values:     0   |    1    |      30        |    3999    | 4000
//     */
////    @Test
////    public void lesstheRange()
////    {
////        num = 0;
////        rc.toRoman(num);
////
////    }
//    @Test
//    public void equaltoLow()
//    {
//        num = 1;
//        rc.toRoman(num);
//    }
//    @Test
//    public void intheRange()
//    {
//        num = 30;
//        rc.toRoman(num);
//    }
//    @Test
//    public void equaltoHigh()
//    {
//        num = 3999;
//        rc.toRoman(num);
//    }
////    @Test
////    public void overtheRange()
////    {
////        num = 4000;
////        rc.toRoman(num);
////    }
//
//
//
//    /***********************  toRoman Tests( Other Extra Test ) ***********************/
//    /*
//    Characteristics: relation between num and 0
//                     num ==0 | num > 0 | num < 0
//             values:     0   |   -1    |  1
//     */
////    @Test
////    public void eqZero()
////    {
////        num = 0;
////        rc.toRoman(num);
////        // assertEquals("L",rc.toRoman(50));
////    }
//    @Test
//    public void gtZero()
//    {
//        num = 1;
//        rc.toRoman(num);
//    }
////    @Test
////    public void ltZero()
////    {
////        num = -1;
////        rc.toRoman(num);
////    }
//   /*
//    Characteristics: relation between num and Interger.MAX_VALUE, can also test on Integer.MIN_VALUE, though
//                     num == Integer.MAX_VALUE | num != Integer.MAX_VALUE
//             values:     Integer.MAX_VALUE   |   10
//     */
////    @Test
////    public void eqMax()
////    {
////        num = Integer.MAX_VALUE;
////        rc.toRoman(num);
////    }
//    @Test
//    public void notEqMax()
//    {
//        num = 10;
//        rc.toRoman(num);
//        //assertEquals("X",rc.toRoman(num));
//
//    }
//
//    /*************************************  fromRoman Tests *****************************************/
//    /*
//    Characteristics: relation between str and null
//                     str == null| str != null
//             values:     null   |  ""
//     */
////    @Test
////    public void nullStr()
////    {
////        str = null;
////        rc.fromRoman(str);
////    }
//    @Test
//    public void notNullStr()
//    {
//        str = "";
//        rc.fromRoman(str);
//
//    }
//
//    /*************************************  fromRoman Tests (Other Extra Test)****************************/
//    /*
//    Characteristics: relation between str and empty
//                     str.isEmpty() | !str.isEmpty()
//             values:           ""  |  "ABC"
//     */
//    @Test
//    public void emptyStr()
//    {
//        str = "";
//        rc.fromRoman(str);
//    }
////    @Test
////    public void notEmptyStr()
////    {
////        str = "ABC";
////        rc.fromRoman(str);
////        //str="X";
////        //assertEquals(10,rc.fromRoman(str));
////    }
//
//}
