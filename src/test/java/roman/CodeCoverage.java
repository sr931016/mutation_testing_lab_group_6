package roman;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class CodeCoverage {
    @Test
    public void test1(){
        String a = "X";
        RomanConverter rc1 = new RomanConverter();
        Assert.assertEquals(rc1.fromRoman(a), 10);
    }
    @Test
    public void test2(){
        int b = 40;
        RomanConverter rc2 = new RomanConverter();
        Assert.assertEquals(rc2.toRoman(b),"XL");
    }

    @Test
    public void test3(){
        int c = 48;
        RomanConverter rc3 = new RomanConverter();
        Assert.assertEquals(rc3.toRoman(c),"XLVIII");

    }

    @Test(expected=IllegalArgumentException.class)
    public void test4(){
        RomanConverter rc4 = new RomanConverter();
        int min = 0;
        TestCase.fail(rc4.toRoman(min));
    }

    @Test(expected=IllegalArgumentException.class)
    public void test5(){
        RomanConverter rc5 = new RomanConverter();
        int max = 4000;
        TestCase.fail(rc5.toRoman(max));
    }

    @Test(expected=IllegalArgumentException.class)
    public void test6() {
        RomanConverter rc6 = new RomanConverter();
        String notroman = "ABC";
        rc6.fromRoman(notroman);
        TestCase.fail();
    }

    @Test
    public void test7(){
        String a = "XII";
        RomanConverter rc1 = new RomanConverter();
        Assert.assertEquals(rc1.fromRoman(a), 12);

    }
}

